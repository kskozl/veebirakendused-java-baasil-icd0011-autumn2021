package control;
import model.Order;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class OrderController {

    public OrderDao dao;

    public OrderController(OrderDao dao) {
        this.dao = dao;
    }

    @PostMapping("orders")
    public Order insertOrder(@RequestBody Order order) {
        return dao.insertOrder(order);
    }

    @GetMapping("orders")
    public List<Order> getAllOrders(){
        return dao.getAllOrders();
    }

    @GetMapping("orders/{id}")
    public Order getOrderForId(@PathVariable Long id){
        return dao.getOrderForId(id);
    }

    @DeleteMapping("orders/{id}")
    public void deleteOrder(@PathVariable Long id){
        dao.deleteOrder(id);
    }



}
