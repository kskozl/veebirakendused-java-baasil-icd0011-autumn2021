package control;

import model.Order;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class OrderDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Order insertOrder(Order order) {
        if (order.id == null){
            em.persist(order);
        } else {
            em.merge(order);
        }
        return order;
    }

    public List<Order> getAllOrders() {
        return em.createQuery("select o from Order o").getResultList();
    }

    public Order getOrderForId(Long id) {
        TypedQuery<Order> query = em.createQuery("select o from Order o where o.id = :id", Order.class);
        query.setParameter("id", id);

        return query.getSingleResult();
    }

    @Transactional
    public void deleteOrder(Long id) {
        Order order = em.find(Order.class, id);
        if (order != null) {
            em.remove(order);
        }
    }
}


