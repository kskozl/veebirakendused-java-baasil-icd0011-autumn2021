package model;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@RequiredArgsConstructor
@Embeddable
@Table(name = "order_rows")
public class Item {

    @Column(name = "item_name")
    public String itemName;

    @NotNull
    @Min(1)
    public int quantity;

    @NotNull
    @Min(1)
    public int price;
}
